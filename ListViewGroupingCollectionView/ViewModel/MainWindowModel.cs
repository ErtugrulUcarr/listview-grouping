﻿using Atomic3;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace LisviewGrouping
{
    public class MainWindowModel : ViewModelBase
    {
        #region ICommand

        private ICommand expanderLoadedCommand;
        private ICommand expandedCommand;
        private ICommand clearGroupCommand;
        private ICommand userSearchCommand;
        private ICommand userAddCommand;
        private ICommand userGroupUpdateCommand;
        private ICommand userUpdateCommand;

        public ICommand ExpandedCommand
        {
            get
            {
                if (expandedCommand == null)
                    expandedCommand = new RelayCommand<object>(Expanded); return expandedCommand;
            }
        }

        public ICommand ExpanderLoadedCommand
        {
            get
            {
                if (expanderLoadedCommand == null)
                    expanderLoadedCommand = new RelayCommand<object>(ExpanderLoaded);
                return expanderLoadedCommand;
            }
        }

        public ICommand ClearGroupCommand
        {
            get
            {
                if (clearGroupCommand == null)
                    clearGroupCommand = new RelayCommand(ClearGroup);
                return clearGroupCommand;
            }
        }

        public ICommand UserSearchCommand
        {
            get
            {
                if (userSearchCommand == null)
                    userSearchCommand = new RelayCommand(UserSearch);
                return userSearchCommand;
            }
        }

        public ICommand UserAddCommand
        {
            get
            {
                if (userAddCommand == null)
                    userAddCommand = new RelayCommand(UserAdd);
                return userAddCommand;
            }
        }

        public ICommand UserGroupUpdateCommand
        {
            get
            {
                if (userGroupUpdateCommand == null)
                    userGroupUpdateCommand = new RelayCommand(UserGroupUpdate);
                return userGroupUpdateCommand;
            }
        }

        public ICommand UserUpdateCommand
        {
            get
            {
                if (userUpdateCommand == null)
                    userUpdateCommand = new RelayCommand(UserUpdate);
                return userUpdateCommand;
            }
        }

        #endregion

        #region Members

        public ObservableCollection<User> contacts = new ObservableCollection<User>();

        private ObservableCollection<User> userList = new ObservableCollection<User>();

        public ObservableCollection<User> UserList
        {
            get { return userList; }
            set { userList = value; NotifyPropertyChanged("UserList"); }
        }

        public ObservableCollection<User> Contacts
        {
            get { return contacts; }
            set { contacts = value; NotifyPropertyChanged("Contacts"); }
        }

        private ICollectionView source;

        public ICollectionView Source
        {
            get { return source; }
            set { source = value; NotifyPropertyChanged("Source"); }
        }

        //private CollectionView CollectionView;

        #endregion

        public MainWindowModel()
        {
            Contacts = new ObservableCollection<User>();
            UserList = new ObservableCollection<User>();

            Source = CollectionViewSource.GetDefaultView(UserList);
            Source.SortDescriptions.Add(new SortDescription("UserName", ListSortDirection.Ascending));

            CreateGroup();

            for (int i = 0; i < 1; i++)
            {
                User u = new User() { UserName = "Ertugrul Uçar", UserId = "P1", UserStatus = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString(), UserGroup.LastContact.ToString()} };
                User u5 = new User() { UserName = "E Uçar", UserId = "P7", UserStatus = "1", UserGroup = new List<string> { UserGroup.Department.ToString(), UserGroup.LastContact.ToString() } };

                User u1 = new User() { UserName = "A Uçar", UserId = "P2", UserStatus = "2", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };
                User u3 = new User() { UserName = "C Uçar", UserId = "P4", UserStatus = "4", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };

                User u4 = new User() { UserName = "D Uçar", UserId = "P5", UserStatus = "1", UserGroup = new List<string> { UserGroup.LastContact.ToString(), UserGroup.Favorite.ToString() } };
                User u2 = new User() { UserName = "B Uçar", UserId = "P3", UserStatus = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };


                Contacts.Add(u);
                Contacts.Add(u1);
                Contacts.Add(u2);
                Contacts.Add(u3);
                Contacts.Add(u4);
                Contacts.Add(u5);

                UserList.Add(u);
                UserList.Add(u1);
                UserList.Add(u2);
                UserList.Add(u3);
                UserList.Add(u4);
                UserList.Add(u5);
            }

            //CreateGroup();
        }

        public void CreateGroup()
        {
            Source.GroupDescriptions.Clear();

            Source = (CollectionView)CollectionViewSource.GetDefaultView(UserList);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("UserGroup");
            Source.GroupDescriptions.Add(groupDescription);
        }

        public void ClearGroup()
        {
            Source.GroupDescriptions.Clear();
        }

        private void UserSearch()
        {
            for (int i = 0; i < UserList.Count; i++)
            {
                if (UserList[i].UserId == "P1")
                {
                    UserList.RemoveAt(i);

                    //Source.Refresh();
                    return;
                }
            }

          

            //Source.GroupDescriptions.Clear();

            //UserList = new ObservableCollection<User>((Contacts.Where(u => u.UserName.Contains("Uçar"))).OrderBy(user => user.UserName));

            ////CollectionView = (CollectionView)CollectionViewSource.GetDefaultView(UserList);

            //GroupDescription GroupDescription = new PropertyGroupDescription("SearchName");
            //GroupDescription.GroupNames.Add("Aranan");

            //Source.GroupDescriptions.Add(GroupDescription);
        }

        private void UserAdd()
        {
            //CreateGroup();

            User u = new User() { UserName = "Osman Gün", UserId = "P6", UserStatus = "1", UserGroup = new List<string> { UserGroup.Test.ToString(), UserGroup.LastContact.ToString(), UserGroup.Department.ToString() } };

            UserList.Add(u);
            Contacts.Add(u);

            //UserList = new ObservableCollection<User>((UserList.OrderBy(user => user.UserName)));
        }

        private void UserUpdate()
        {
            for (int i = 0; i < UserList.Count; i++)
            {
                if (UserList[i].UserId == "P1")
                {
                    UserList[i].UserStatus = "ffffff";
                }
            }
        }

        private void UserGroupUpdate()
        {
            for (int i = 0; i < UserList.Count; i++)
            {
                if (UserList[i].UserId == "P1")
                {
                    User tempUser = UserList[i];
                    tempUser.UserStatus = "15";

                    tempUser.UserGroup.Remove(UserGroup.Favorite.ToString());
                    //tempUser.UserGroup.Add(UserGroup.Department.ToString());

                    if (tempUser.UserGroup.Where(gr => gr.Contains(UserGroup.Department.ToString())).Count() == 0)
                        tempUser.UserGroup.Add(UserGroup.Department.ToString());

                    UserList.RemoveAt(i);
                    UserList.Add(tempUser);

                    return;

                    //UserList[i].UserStatus = "15";

                    //UserList[i].UserGroup.Remove(UserGroup.Favorite.ToString());

                    //if (UserList[i].UserGroup.Where(gr => gr.Contains(UserGroup.Department.ToString())).Count() == 0)
                    //    UserList[i].UserGroup.Add(UserGroup.Department.ToString());

                    //FavoriteList = new ObservableCollection<User>((MainUserList.Where(user => user.UserGroup.Where(gr => !gr.Contains(UserGroup.Favorite.ToString())).Any())).OrderBy(user => user.Name));
                    //DepartmentList= new ObservableCollection<User>((MainUserList.Where(user => user.UserGroup.Where(gr => !gr.Contains(UserGroup.Department.ToString())).Any())).OrderBy(user => user.Name));
                }
            }

            //UserList = new ObservableCollection<User>((UserList.OrderBy(user => user.UserName)));

            //Source.Refresh();
            //CreateGroup();
        }

        #region Expander

        public void Expanded(object parameter)
        {
        }

        private void ExpanderLoaded(object parameter)
        {
        }

        #endregion
    }
}
