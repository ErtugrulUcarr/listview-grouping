﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Markup;
using System.Windows;
using System.Windows.Media.Animation;

namespace Atomic3._1.Common
{
	public sealed class CommandTriggerGroup : FreezableCollection<CommandTrigger>, ICommandTrigger
	{
		private readonly HashSet<ICommandTrigger> _initList = new HashSet<ICommandTrigger>();		

		void ICommandTrigger.Initialize(FrameworkElement source)
		{
			foreach (ICommandTrigger child in this)
			{
				if (!_initList.Contains(child))
				{
					InitializeCommandSource(source, child);
				}
			}
		}

		private void InitializeCommandSource(FrameworkElement source, ICommandTrigger child)
		{
			child.Initialize(source);			
			_initList.Add(child);
		}
	}
}
