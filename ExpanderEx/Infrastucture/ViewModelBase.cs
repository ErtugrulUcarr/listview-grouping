﻿using Atomic3.InstantMessaging;

namespace Atomic3
{
    /// <summary>
    /// Provides a base class for Applications to inherit from.
    /// </summary>
    public abstract class ViewModelBase
        : ObservableObject
    {
        #region · Constructors ·

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModelBase"/> class.
        /// </summary>
        protected ViewModelBase()
            : base()
        {
        }

        #endregion · Constructors ·
    }
}