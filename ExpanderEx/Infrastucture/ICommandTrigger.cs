﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Atomic3._1.Common
{
	public interface ICommandTrigger
	{
		void Initialize(FrameworkElement source);
	}
}
