﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1;

namespace ExpanderEx
{

    public class User : IUser, INotifyPropertyChanged
    {
        public Department Department { get; set; }

        List<string> userGroup;

        public List<string> UserGroup
        {
            get
            {
                return userGroup;
            }
            set
            {
                userGroup = value;
                OnPropertyChanged(new PropertyChangedEventArgs("UserGroup"));
            }
        }

        string userId;
        string name;
        string statu;

        public string UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
                OnPropertyChanged(new PropertyChangedEventArgs("UserId"));
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Name"));
            }
        }

        public string UserStatu
        {
            get
            {
                return statu;
            }
            set
            {
                statu = value;
                OnPropertyChanged(new PropertyChangedEventArgs("UserStatu"));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
    }

    //[Flags]
    public enum UserGroup
    {
        Favorite = 1,
        Department = 2,
        LastContact = 3,
        Aranan,
    }

    [Flags]
    public enum Department
    {
        None = 0,
        A = 1,
        B = 2,
        C = 4,
        D = 8
    }
}
