﻿using Atomic3;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApplication1;

namespace ExpanderEx
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {



        private MainWindowModel MainWindowModel = null;

        public MainWindow()
        {
            InitializeComponent();
            MainWindowModel = new MainWindowModel();
            this.DataContext = MainWindowModel;
        }



        //private User NewUser(User item, List<string> group)
        //{
        //    //User founduser = MainUserList.Where(u => u.UserId == item.UserId).FirstOrDefault();
        //    //founduser.UserGroup = group;

        //    //return founduser;

        //    User user = new User();
        //    user.Name = item.Name;
        //    user.UserGroup = group;
        //    user.UserId = item.UserId;
        //    user.UserStatu = item.UserStatu;
        //    return user;
        //}

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Button_Click(sender, e);
        }

        /// <summary>
        /// Online
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Button_Click_1(sender, e);

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Button_Click_2(sender, e);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Button_Click_3(sender, e);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            MainWindowModel.Button_Click_4(sender, e);
        }
    }


    public class MainWindowModel : ViewModelBase
    {
        CollectionView view;

        private ICommand expanderLoadedCommand;
        private ICommand expandedCommand;

        public ICommand ExpandedCommand
        {
            get
            {
                if (expandedCommand == null)
                    expandedCommand = new RelayCommand<object>(Expanded); return expandedCommand;
            }
        }

        public ICommand ExpanderLoadedCommand
        {
            get
            {
                if (expanderLoadedCommand == null)
                    expanderLoadedCommand = new RelayCommand<object>(ExpanderLoaded);
                return expanderLoadedCommand;
            }
        }

        ObservableCollection<User> mainUserList = new ObservableCollection<User>();

        public ObservableCollection<User> MainUserList
        {
            get { return mainUserList; }
            set { mainUserList = value; NotifyPropertyChanged(new PropertyChangedEventArgs("MainUserList")); }
        }

        public ObservableCollection<User> ContactList;


        private bool isExpand = true;

        public MainWindowModel()
        {
            MainUserList = new ObservableCollection<User>();
            ContactList = new ObservableCollection<User>();

            for (int i = 0; i < 1000; i++)
            {
                User u = new User() { Name = "Ertugrul Uçar", UserId = "P1", UserStatu = "1", UserGroup = new List<string> { UserGroup.Aranan.ToString(), UserGroup.Favorite.ToString() } };
                User u11 = new User() { Name = "C Uçar", UserId = "P1", UserStatu = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };
                User u22 = new User() { Name = "N Uçar", UserId = "P1", UserStatu = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };
                User u33 = new User() { Name = "a Uçar", UserId = "P1", UserStatu = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };


                User u1 = new User() { Name = "Eko Uçar", UserId = "P1", UserStatu = "2", UserGroup = new List<string> { UserGroup.Department.ToString() } };

                User u2 = new User() { Name = "Ertugrul", UserId = "P1", UserStatu = "3", UserGroup = new List<string> { UserGroup.LastContact.ToString() } };

                MainUserList.Add(u);
                MainUserList.Add(u11);
                MainUserList.Add(u22);
                MainUserList.Add(u33);

                MainUserList.Add(u1);
                MainUserList.Add(u2);

                ContactList.Add(u);
                ContactList.Add(u11);
                ContactList.Add(u22);
                ContactList.Add(u33);

                ContactList.Add(u1);
                ContactList.Add(u2);
            }

            CreateGroup();
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            User u = new User() { Name = "Ertugrul Uçar", UserId = "P1", UserStatu = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString(), UserGroup.LastContact.ToString(), UserGroup.Department.ToString() } };

            MainUserList.RemoveAt(0);

            MainUserList.Add(u);
            //MainUserList[0].UserGroup.Add(UserGroup.LastContact.ToString());


            for (int i = 0; i < MainUserList.Count; i++)
            {
                if (MainUserList[i].UserId == "P1")
                {
                    MainUserList[i].UserStatu = "5";
                    //FavoriteList = new ObservableCollection<User>((MainUserList.Where(user => user.UserGroup.Where(gr => !gr.Contains(UserGroup.Favorite.ToString())).Any())).OrderBy(user => user.Name));
                    //DepartmentList= new ObservableCollection<User>((MainUserList.Where(user => user.UserGroup.Where(gr => !gr.Contains(UserGroup.Department.ToString())).Any())).OrderBy(user => user.Name));
                }
            }

            //MainUserList = MainUserList;

            //view.Refresh();
        }

        //Online
        public void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainUserList = new ObservableCollection<User>((ContactList.Where(u => u.UserStatu == "1")).OrderBy(user => user.Name));

            CreateGroup();
        }

        public void Button_Click_2(object sender, RoutedEventArgs e)
        {
            MainUserList = new ObservableCollection<User>((ContactList.Where(u => u.UserStatu == "2")).OrderBy(user => user.Name)); ;

            CreateGroup();
        }

        public void Button_Click_3(object sender, RoutedEventArgs e)
        {
            MainUserList = new ObservableCollection<User>((ContactList.Where(u => u.UserStatu == "3")).OrderBy(user => user.Name)); ;

            CreateGroup();
        }

        private void CreateGroup()
        {

            view = (CollectionView)CollectionViewSource.GetDefaultView(MainUserList);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("UserGroup");
            view.GroupDescriptions.Add(groupDescription);

            //foreach (var item in view.Groups)
            //{

            //    //CollectionViewGroupInternal
            //    //MS.Internal.Data.CollectionViewGroupInternal


            //    Type type = item.GetType();
            //    PropertyInfo pi = type.GetProperty("SeedItem", BindingFlags.NonPublic | BindingFlags.Instance);
            //    var a = pi.GetValue(item, null);

            //    //item
            //    //MS.Internal.Data.CollectionViewGroupInternal

            //}

            PropertyGroupDescription groupDescriptionSearch = new PropertyGroupDescription("Aranan");
            view.GroupDescriptions.Remove(groupDescriptionSearch);
        }

        private void CreateGroupSearch()
        {
            view = (CollectionView)CollectionViewSource.GetDefaultView(MainUserList);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("UserGroup");
            view.GroupDescriptions.Clear();
            view.GroupDescriptions.Add(groupDescription);


            //PropertyGroupDescription groupDescription0 = new PropertyGroupDescription(UserGroup.Department.ToString());
            //PropertyGroupDescription groupDescription1 = new PropertyGroupDescription(UserGroup.Favorite.ToString());
            //PropertyGroupDescription groupDescription2 = new PropertyGroupDescription(UserGroup.LastContact.ToString());
            //view.GroupDescriptions.Remove(groupDescription0);
            //view.GroupDescriptions.Remove(groupDescription1);
            //view.GroupDescriptions.Remove(groupDescription2);

        }

        public void Button_Click_4(object sender, RoutedEventArgs e)
        {
            MainUserList = new ObservableCollection<User>((MainUserList.Where(u => u.Name.Contains("Ertugrul"))).OrderBy(user => user.Name));

            CreateGroupSearch();

            //view.GroupDescriptions.Clear();

            //view.GroupDescriptions.Add("Aranan");


        }

        public bool IsExpand
        {
            get { return isExpand; }
            set { isExpand = value; NotifyPropertyChanged(new PropertyChangedEventArgs("IsExpand")); }
        }

        public void Expanded(object parameter)
        {
            var exp = parameter as Expander;
            var dc = exp.DataContext as CollectionViewGroup;


            //ExpandDictionary[dc.Name.ToString()] = exp.IsExpanded;
        }

        private void ExpanderLoaded(object parameter)
        {
        }
    }

}