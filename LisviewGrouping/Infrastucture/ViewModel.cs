﻿using System.ComponentModel;

namespace Atomic3
{
    /// <summary>
    /// Base class for ViewModel implementations
    /// </summary>
    /// <typeparam name="TDataModel"></typeparam>
    /// <remarks>Model-View-ViewModel pattern</remarks>
    public abstract class ViewModel<TDataModel>
        : ViewModelBase, IDataErrorInfo
    {
        #region · Fields ·

        private TDataModel dataModel;

        #endregion · Fields ·

        #region · IDataErrorInfo Members ·

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get { return null; }
        }

        #endregion · IDataErrorInfo Members ·

        #region · Protected Properties ·

        /// <summary>
        /// Gets the data model.
        /// </summary>
        /// <value>The data model.</value>
        protected TDataModel DataModel
        {
            get { return this.dataModel; }
            set { this.dataModel = value; }
        }

        #endregion · Protected Properties ·

        #region · Constructors ·

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        protected ViewModel()
            : base()
        {
        }

        #endregion · Constructors ·
    }
}