﻿using Atomic3;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace LisviewGrouping
{
    public class MainWindowModel : ViewModelBase
    {
        #region ICommand

        private ICommand expanderLoadedCommand;
        private ICommand expandedCommand;
        private ICommand clearGroupCommand;
        private ICommand userSearchCommand;
        private ICommand userAddCommand;
        private ICommand userGroupUpdateCommand;

        public ICommand ExpandedCommand
        {
            get
            {
                if (expandedCommand == null)
                    expandedCommand = new RelayCommand<object>(Expanded); return expandedCommand;
            }
        }

        public ICommand ExpanderLoadedCommand
        {
            get
            {
                if (expanderLoadedCommand == null)
                    expanderLoadedCommand = new RelayCommand<object>(ExpanderLoaded);
                return expanderLoadedCommand;
            }
        }

        public ICommand ClearGroupCommand
        {
            get
            {
                if (clearGroupCommand == null)
                    clearGroupCommand = new RelayCommand(ClearGroup);
                return clearGroupCommand;
            }
        }

        public ICommand UserSearchCommand
        {
            get
            {
                if (userSearchCommand == null)
                    userSearchCommand = new RelayCommand(UserSearch);
                return userSearchCommand;
            }
        }

        public ICommand UserAddCommand
        {
            get
            {
                if (userAddCommand == null)
                    userAddCommand = new RelayCommand(UserAdd);
                return userAddCommand;
            }
        }

        public ICommand UserGroupUpdateCommand
        {
            get
            {
                if (userGroupUpdateCommand == null)
                    userGroupUpdateCommand = new RelayCommand(UserGroupUpdate);
                return userGroupUpdateCommand;
            }
        }

        #endregion

        #region Members

        public ObservableCollection<User> contacts = new ObservableCollection<User>();

        private ObservableCollection<User> userList = new ObservableCollection<User>();

        public ObservableCollection<User> UserList
        {
            get { return userList; }
            set { userList = value; NotifyPropertyChanged("UserList"); }
        }

        public ObservableCollection<User> Contacts
        {
            get { return contacts; }
            set { contacts = value; NotifyPropertyChanged("Contacts"); }
        }

        //public ICollectionView Source { get; set; }

        private CollectionView CollectionView;

        #endregion

        public MainWindowModel()
        {
            Contacts = new ObservableCollection<User>();
            UserList = new ObservableCollection<User>();

            //Source = CollectionViewSource.GetDefaultView(UserList);

            for (int i = 0; i < 10; i++)
            {
                User u = new User() { UserName = "Ertugrul Uçar", UserId = "P1", UserStatus = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };
                User u1 = new User() { UserName = "A Uçar", UserId = "P2", UserStatus = "2", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };
                User u2 = new User() { UserName = "B Uçar", UserId = "P3", UserStatus = "1", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };
                User u3 = new User() { UserName = "C Uçar", UserId = "P4", UserStatus = "4", UserGroup = new List<string> { UserGroup.Favorite.ToString() } };

                User u4 = new User() { UserName = "D Uçar", UserId = "P5", UserStatus = "1", UserGroup = new List<string> { UserGroup.LastContact.ToString(), UserGroup.Favorite.ToString() } };

                User u5 = new User() { UserName = "E Uçar", UserId = "P6", UserStatus = "1", UserGroup = new List<string> { UserGroup.Department.ToString(), UserGroup.LastContact.ToString() } };

                Contacts.Add(u);
                Contacts.Add(u1);
                Contacts.Add(u2);
                Contacts.Add(u3);
                Contacts.Add(u4);
                Contacts.Add(u5);

                UserList.Add(u);
                UserList.Add(u1);
                UserList.Add(u2);
                UserList.Add(u3);
                UserList.Add(u4);
                UserList.Add(u5);
            }

            CreateGroup();
        }

        public void CreateGroup()
        {
            CollectionView = (CollectionView)CollectionViewSource.GetDefaultView(UserList);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("UserGroup");
            CollectionView.GroupDescriptions.Add(groupDescription);
        }

        public void ClearGroup()
        {
            CollectionView.GroupDescriptions.Clear();
        }

        private void UserSearch()
        {
            CollectionView.GroupDescriptions.Clear();

            UserList = new ObservableCollection<User>((Contacts.Where(u => u.UserName.Contains("Uçar"))).OrderBy(user => user.UserName));

            CollectionView = (CollectionView)CollectionViewSource.GetDefaultView(UserList);

            GroupDescription GroupDescription = new PropertyGroupDescription("SearchName");
            GroupDescription.GroupNames.Add("Aranan");

            CollectionView.GroupDescriptions.Add(GroupDescription);
        }

        private void UserAdd()
        {
            User u = new User() { UserName = "Osman Gün", UserId = "P6", UserStatus = "1", UserGroup = new List<string> { UserGroup.Test.ToString(), UserGroup.LastContact.ToString(), UserGroup.Department.ToString() } };

            UserList.Add(u);
            Contacts.Add(u);
        }

        private void UserGroupUpdate()
        {
            for (int i = 0; i < Contacts.Count; i++)
            {
                if (Contacts[i].UserId == "P1")
                {
                    Contacts[i].UserStatus = "15";

                    Contacts[i].UserGroup.Remove(UserGroup.Favorite.ToString());

                    if (Contacts[i].UserGroup.Where(gr => gr.Contains(UserGroup.Department.ToString())).Count() == 0)
                        Contacts[i].UserGroup.Add(UserGroup.Department.ToString());

                    //FavoriteList = new ObservableCollection<User>((MainUserList.Where(user => user.UserGroup.Where(gr => !gr.Contains(UserGroup.Favorite.ToString())).Any())).OrderBy(user => user.Name));
                    //DepartmentList= new ObservableCollection<User>((MainUserList.Where(user => user.UserGroup.Where(gr => !gr.Contains(UserGroup.Department.ToString())).Any())).OrderBy(user => user.Name));
                }
            }

            UserList = new ObservableCollection<User>((Contacts.OrderBy(user => user.UserName)));

            //CollectionView.Refresh();
            CreateGroup();

        }

        #region Expander

        public void Expanded(object parameter)
        {
        }

        private void ExpanderLoaded(object parameter)
        {
        }

        #endregion
    }
}
