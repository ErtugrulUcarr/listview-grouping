﻿using Atomic3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication1;

public class User : ViewModelBase, IUser
{
    List<string> userGroup;

    public List<string> UserGroup
    {
        get
        {
            return userGroup;
        }
        set
        {
            userGroup = value;
            NotifyPropertyChanged("UserGroup");
        }
    }

    private string searchName = "Aranan";

    public string SearchName
    {
        get { return searchName; }
        set { searchName = value; NotifyPropertyChanged("SearchName"); }
    }

    string userId;
    string userName;
    string status;

    public string UserId
    {
        get
        {
            return userId;
        }
        set
        {
            userId = value;
            NotifyPropertyChanged(new PropertyChangedEventArgs("UserId"));
        }
    }

    public string UserName
    {
        get
        {
            return userName;
        }
        set
        {
            userName = value;
            NotifyPropertyChanged(new PropertyChangedEventArgs("UserName"));
        }
    }

    public string UserStatus
    {
        get
        {
            return status;
        }
        set
        {
            status = value;
            NotifyPropertyChanged(new PropertyChangedEventArgs("UserStatus"));
        }
    }
}

public enum UserGroup
{
    Favorite = 1,
    Department = 2,
    LastContact = 3,
    Test,
}
